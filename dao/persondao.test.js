var mysql = require("mysql");

const PersonDao = require("./persondao.js");
const runsqlfile = require("./runsqlfile.js");

//fkoXvi9W
// GitLab CI Pool
var pool = mysql.createPool({
  connectionLimit: 1,
  host: "mysql.stud.iie.ntnu.no",
  user: "terhaug",
  password: "nUCf6nCK",
  database: "terhaug",
  debug: false,
  multipleStatements: true
});

let personDao = new PersonDao(pool);

beforeAll(done => {
  runsqlfile("dao/create_tables.sql", pool, () => {
    runsqlfile("dao/create_testdata.sql", pool, done);
  });
});

afterAll(() => {
  pool.end();
});

test("get one person from db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    expect(data.length).toBe(1);
    expect(data[0].navn).toBe("Hei Sveisen");
    done();
  }

  personDao.getOne(1, callback);
});

test("get unknown person from db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    expect(data.length).toBe(0);
    done();
  }

  personDao.getOne(0, callback);
});

test("add person to db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    expect(data.affectedRows).toBeGreaterThanOrEqual(1);
    done();
  }

  personDao.createOne(
    { navn: "Nils Nilsen", alder: 34, adresse: "Gata 3" },
    callback
  );
});

test("get all persons from db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data.length=" + data.length
    );
    expect(data.length).toBeGreaterThanOrEqual(2);
    done();
  }

  personDao.getAll(callback);
});

test("update person", done => {
  personDao.getOne(1, (status, before_data) => {
    personDao.updateOne(1, {navn: before_data[0].navn, alder: before_data[0].alder + 1, adresse: before_data[0].adresse}, (status, data) => {
      personDao.getOne(1, (status, after_data) => {
        expect(after_data[0].navn).toEqual(before_data[0].navn);
        expect(after_data[0].adresse).toEqual(before_data[0].adresse);
        expect(after_data[0].alder).not.toBe(before_data[0].alder);
        done();
      });
    });
  });
});

test("delete person", done => {
  personDao.getAll((status, before_data) => {
    personDao.deleteOne(2, (status, data) => {
      personDao.getAll((status, after_data) => {
        expect(before_data.length).not.toBe(after_data.length);
        done();
      });
    });
  });
});
